import os
import unittest

from click.testing import CliRunner

from kachlog.commands import cli


class CliIntegrationTestCase(unittest.TestCase):
    def setUp(self):
        self.runner = CliRunner()
        os.environ.setdefault("LC_ALL", "en_US.utf-8")
        os.environ.setdefault("LANG", "en_US.utf-8")

    def test_cli_init(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["init"])
            self.assertTrue(os.path.isfile("CHANGELOG.md"))
            with open("CHANGELOG.md", "r") as ch:
                data = ch.readlines()
            self.assertFalse("### Added\n" in data)
            self.assertFalse("### Changed\n" in data)
            self.assertFalse("### Deprecated\n" in data)
            self.assertFalse("### Removed\n" in data)
            self.assertFalse("### Fixed\n" in data)
            self.assertFalse("### Security\n" in data)
            self.assertTrue(result)

    def test_cli_init_empty_sections(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["init", "--sections"])
            self.assertTrue(os.path.isfile("CHANGELOG.md"))
            with open("CHANGELOG.md", "r") as ch:
                data = ch.readlines()
            self.assertTrue("### Added\n" in data)
            self.assertTrue("### Changed\n" in data)
            self.assertTrue("### Deprecated\n" in data)
            self.assertTrue("### Removed\n" in data)
            self.assertTrue("### Fixed\n" in data)
            self.assertTrue("### Security\n" in data)
            self.assertTrue(result)

    def test_cli_current(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["current"])
            self.assertEqual(result.output.strip(), "0.0.0")

    def test_cli_current_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["current"])
            self.assertEqual(result.output.strip(), "")

    def test_cli_suggest(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["suggest"])
            self.assertEqual(result.output.strip(), "0.1.0")

    def test_cli_suggest_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["suggest"])
            self.assertEqual(result.output.strip(), "")

    def test_cli_version_flag(self):
        result = self.runner.invoke(cli, ["--version"])
        self.assertTrue(result)

    def test_cli_added(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["added", "Adding a new feature"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["suggest"])
            self.assertEqual(suggest.output.strip(), "0.1.0")

    def test_cli_added_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(
                cli, ["added", "Adding a new feature"], input="y\n"
            )
            self.assertEqual(
                result.output.strip(),
                "No CHANGELOG.md found, do you want to create one? [y/N]: y",
            )

    def test_cli_changed(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["changed", "Changing a feature"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["suggest"])
            self.assertEqual(suggest.output.strip(), "1.0.0")

    def test_cli_changed_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(
                cli, ["changed", "changing a feature"], input="y\n"
            )
            self.assertEqual(
                result.output.strip(),
                "No CHANGELOG.md found, do you want to create one? [y/N]: y",
            )

    def test_cli_fixed(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["fixed", "Fix a Bug"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["suggest"])
            self.assertEqual(suggest.output.strip(), "0.0.1")

    def test_cli_suggest_type_fixed(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["fixed", "Fix a Bug"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["suggest", "--type"])
            self.assertEqual(suggest.output.strip(), "patch")

    def test_cli_fixed_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["fixed", "Fix a Bug"], input="y\n")
            self.assertEqual(
                result.output.strip(),
                "No CHANGELOG.md found, do you want to create one? [y/N]: y",
            )

    def test_cli_removed(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["removed", "Breaking Change"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["suggest"])
            self.assertEqual(suggest.output.strip(), "1.0.0")

    def test_cli_suggest_type_removed(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            result = self.runner.invoke(cli, ["removed", "Breaking Change"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["suggest", "--type"])
            self.assertEqual(suggest.output.strip(), "major")

    def test_cli_removed_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(
                cli, ["removed", "Breaking Change"], input="y\n"
            )
            self.assertEqual(
                result.output.strip(),
                "No CHANGELOG.md found, do you want to create one? [y/N]: y",
            )

    def test_cli_release(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release"])
            self.assertEqual(
                result.output.strip(),
                "Planning on releasing version 0.1.0. Proceed? [y/N]:",
            )

    def test_cli_release_confirm(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release"], "y")
            self.assertEqual(result.output.strip().split("\n")[-1], "Released 0.1.0")

    def test_cli_release_nonexistent(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["release"], "y")
            self.assertTrue(os.path.isfile("CHANGELOG.md"))

    def test_cli_release_y(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release", "--yes"])
            self.assertTrue(result)
            suggest = self.runner.invoke(cli, ["current"])
            self.assertEqual(suggest.output.strip(), "0.1.0")

    def test_cli_release_y_specify_type(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release", "--major", "--yes"])
            self.assertTrue(result)
            current = self.runner.invoke(cli, ["current"])
            self.assertEqual(current.output.strip(), "1.0.0")

    def test_cli_release_y_specify_version(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release", "--custom", "4.0.0", "--yes"])
            self.assertTrue(result)
            current = self.runner.invoke(cli, ["current"])
            self.assertEqual(current.output.strip(), "4.0.0")

    def test_cli_release_y_specify_version_non_semantic(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(
                cli, ["release", "--custom", "non-semantic-version", "--yes"]
            )
            self.assertTrue(result)
            self.assertEqual(
                result.output.strip(),
                "non-semantic-version is not following semantic versioning. "
                "Check https://semver.org for more information.",
            )

    def test_cli_release_y_specify_version_and_type(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(
                cli, ["release", "--custom", "4.0.0", "--yes", "--minor"]
            )
            self.assertTrue(result)
            self.assertEqual(
                result.output.strip().split("\n")[0],
                "WARNING: custom version precedes release_type flag",
            )
            current = self.runner.invoke(cli, ["current"])
            self.assertEqual(current.output.strip(), "4.0.0")

    def test_cli_release_y_lower_version(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release", "--major", "--yes"])
            self.assertTrue(result)
            self.assertEqual(result.output.strip(), "Released 1.0.0")
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["release", "--custom", "0.1.0", "--yes"])
            self.assertTrue(result)
            self.assertEqual(
                result.output.strip(),
                "0.1.0 is not following semantic versioning. Check https://semver.org for more information.",
            )

    def test_cli_release_missing(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["release"])
            self.assertEqual(
                result.output.strip(),
                "No CHANGELOG.md found, do you want to create one? [y/N]:",
            )

    def test_cli_view(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["init"])
            self.runner.invoke(cli, ["added", "Adding a new feature"])
            result = self.runner.invoke(cli, ["view"])
            self.assertTrue(result)
            result = self.runner.invoke(cli, ["release", "--yes"])
            result = self.runner.invoke(cli, ["view"])
            self.assertTrue(result)

    def test_cli_view_nonexistent(self):
        with self.runner.isolated_filesystem():
            result = self.runner.invoke(cli, ["view"], "y")
            self.assertTrue(result)
            self.assertTrue(os.path.isfile("CHANGELOG.md"))

    def test_cli_shorthand_commands(self):
        with self.runner.isolated_filesystem():
            self.runner.invoke(cli, ["i"])
            with open("CHANGELOG.md", "r", encoding="utf8") as ch:
                data = ch.readlines()
            self.assertFalse("### Added\n" in data)
            self.assertFalse("### Changed\n" in data)
            self.assertFalse("### Deprecated\n" in data)
            self.assertFalse("### Removed\n" in data)
            self.assertFalse("### Fixed\n" in data)
            self.assertFalse("### Security\n" in data)
            self.runner.invoke(cli, ["a", "Adding a new feature"])
            with open("CHANGELOG.md", "r", encoding="utf8") as ch:
                data = ch.readlines()
            self.assertTrue("### Added\n" in data)
            result = self.runner.invoke(cli, ["c", "Changed behavior"])
            self.assertEqual(
                result.output.strip(),
                "Too many command matches. Did you mean: changed, current\nAborted!",
            )
            result = self.runner.invoke(cli, ["command"])
            self.assertEqual(
                result.output.strip().split("\n")[-1],
                "Error: No such command 'command'.",
            )
