import os
import unittest
from datetime import date
from unittest.mock import PropertyMock, patch

from kachlog._changelog import _to_dict, from_dict, release, to_dict, to_raw_dict
from kachlog._versioning import (
    bump_major,
    bump_minor,
    bump_patch,
    from_semantic,
    to_semantic,
)
from kachlog.exceptions import (
    ChangelogDoesNotExistError,
    InvalidSemanticVersion,
    VersionDoesNotExistError,
)
from kachlog.utils import ChangelogUtils

CHANGELOG_DATA = """# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Release note 1. 
* Release note 2.

### Added
- Enhancement 1
 - sub enhancement 1 
 * sub enhancement 2
- Enhancement 2

### Fixed
- Bug fix 1
 - sub bug 1
 * sub bug 2
- Bug fix 2

### Security
* Known issue 1
- Known issue 2

### Deprecated
- Deprecated feature 1 
* Future removal 2

### Removed
- Deprecated feature 2
* Future removal 1 

## [1.1.0] - 2018-05-31
### Changed
- Enhancement 1 (1.1.0)
- sub *enhancement 1*
- sub enhancement 2
- Enhancement 2 (1.1.0)

## [1.1.0-dev0] - 2018-05-31
### Changed
- Enhancement 1 (1.1.0)
- sub *enhancement 1*
- sub enhancement 2
- Enhancement 2 (1.1.0)

## [1.0.1] - 2018-05-31

- uncategorized log

### Fixed
- Bug fix 1 (1.0.1)
- sub bug 1
- sub bug 2
- Bug fix 2 (1.0.1)

## [1.0.0] - 2017-04-10
### Deprecated
- Known issue 1 (1.0.0)
- Known issue 2 (1.0.0)

[Unreleased]: https://github.test_url/test_project/compare/v1.1.0...HEAD
[1.1.0]: https://github.test_url/test_project/compare/v1.0.1...v1.1.0
[1.0.1]: https://github.test_url/test_project/compare/v1.0.0...v1.0.1
[1.0.0]: https://github.test_url/test_project/releases/tag/v1.0.0
"""


class UtilsTestCase(unittest.TestCase):
    def setUp(self):
        self.cl = ChangelogUtils()
        self.cl.CHANGELOG = "CHANGELOG_TEST.md"

    def tearDown(self):
        try:
            os.remove(self.cl.CHANGELOG)
        except Exception:
            pass

    def test_initial_version(self):
        version = to_semantic(None)
        self.assertEqual(from_semantic(version), "0.0.0")

    def test_bump_version_major(self):
        original = "0.1.0"
        bumped = to_semantic(original)
        bump_major(bumped)
        self.assertEqual(from_semantic(bumped), "1.0.0")

    def test_bump_version_minor(self):
        original = "0.1.0"
        bumped = to_semantic(original)
        bump_minor(bumped)
        self.assertEqual(from_semantic(bumped), "0.2.0")

    def test_bump_version_patch(self):
        original = "0.1.0"
        bumped = to_semantic(original)
        bump_patch(bumped)
        self.assertEqual(from_semantic(bumped), "0.1.1")

    def test_get_release_suggestion_patch(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "fixed": ["fix bug"],
            }
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.get_release_suggestion()
        self.assertEqual(result, "patch")

    def test_get_release_suggestion_minor(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "added": ["added code"],
            }
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.get_release_suggestion()
        self.assertEqual(result, "minor")

    def test_get_release_suggestion_major(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "removed": ["stuff"],
            }
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.get_release_suggestion()
        self.assertEqual(result, "major")

    def test_update_section(self):
        message = "this is a test"
        with patch.object(
            ChangelogUtils,
            "data",
            new_callable=PropertyMock,
            return_value={
                "unreleased": {
                    "metadata": {"version": "unreleased", "release_date": None}
                }
            },
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            CL.update_section("added", message)
            self.assertEqual(CL.data["unreleased"]["added"], [message])
            CL.update_section("added", message)
            self.assertEqual(CL.data["unreleased"]["added"], [message, message])

    def test_get_current_version(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "new": ["added new feature"],
                "changes": ["changes feature"],
                "fixes": ["fixed bug 1"],
                "breaks": [],
            },
            "0.3.2": {
                "metadata": {
                    "version": "0.3.2",
                    "release_date": "2017-06-09",
                    "semantic_version": {"major": 0, "minor": 3, "patch": 2},
                }
            },
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.get_current_version()
        self.assertEqual(result, "0.3.2")

    def test_compare_version_prerelease_buildmetadata(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "new": ["added new feature"],
                "changes": ["changes feature"],
                "fixes": ["fixed bug 1"],
                "breaks": [],
            },
            "0.3.2-alpha2+my.build": {
                "metadata": {
                    "version": "0.3.2-alpha2+my.build",
                    "release_date": "2017-06-09",
                    "semantic_version": {
                        "major": 0,
                        "minor": 3,
                        "patch": 2,
                        "prerelease": "alpha2",
                        "buildmetadata": "my.build",
                    },
                }
            },
            "0.3.2-alpha1+my.build": {
                "metadata": {
                    "version": "0.3.2-alpha1+my.build",
                    "release_date": "2017-06-09",
                    "semantic_version": {
                        "major": 0,
                        "minor": 3,
                        "patch": 2,
                        "prerelease": "alpha1",
                        "buildmetadata": "my.build",
                    },
                }
            },
            "0.3.1": {
                "metadata": {
                    "version": "0.3.1",
                    "release_date": "2017-06-09",
                    "semantic_version": {
                        "major": 0,
                        "minor": 3,
                        "patch": 1,
                        "prerelease": None,
                        "buildmetadata": None,
                    },
                }
            },
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.get_current_version()
        self.assertEqual(result, "0.3.2-alpha2+my.build")

    def test_get_current_version_default(self):
        sample_data = {
            "unreleased": {"metadata": {"version": "unreleased", "release_date": None}}
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.get_current_version()
        self.assertEqual(result, "0.0.0")

    def test_get_changes(self):
        sample_data = [
            "## [Unreleased]\n",
            "\n",
            "### Added\n",
            "* added feature x\n",
            "\n",
            "### Fixed\n",
            "* fixed bug 1\n",
            "\n",
            "### Removed\n",
            "\n",
            "\n",
            "## [0.3.2] - 2017-06-09\n",
            "\n",
        ]
        with patch.object(
            ChangelogUtils,
            "data",
            new_callable=PropertyMock,
            return_value=_to_dict(sample_data, show_unreleased=True),
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.data["unreleased"]
        self.assertTrue("added" in result)
        self.assertTrue("fixed" in result)

    def test_get_changes_works_with_old_headers(self):
        sample_data = [
            "## [Unreleased]\n",
            "\n",
            "### New\n",
            "* added feature x\n",
            "\n",
            "### Changes\n",
            "* changes feature x\n",
            "\n",
            "### Fixes\n",
            "* fixed bug 1\n",
            "\n",
            "### Breaks\n",
            "\n",
            "\n",
            "## [0.3.2] - 2017-06-09\n",
            "\n",
        ]
        with patch.object(
            ChangelogUtils,
            "data",
            new_callable=PropertyMock,
            return_value=_to_dict(sample_data, show_unreleased=True),
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.cl.CHANGELOG
            result = CL.data["unreleased"]
        self.assertTrue("new" in result)
        self.assertTrue("changes" in result)
        self.assertTrue("fixes" in result)

    @patch("kachlog.utils.actual_version")
    def test_bump_prereleaase_and_metadata_version(self, mock_actual_version):
        mock_actual_version.return_value = (
            "1.1.1-alpha+local_build",
            {
                "major": 1,
                "minor": 1,
                "patch": 1,
                "prerelease": "alpha",
                "buildmetadata": "local_build",
            },
        )
        CL = ChangelogUtils()
        CL.CHANGELOG = self.cl.CHANGELOG
        CL.initialize_changelog_file()
        self.assertEqual(CL.get_new_release_version(), "1.1.1")

    @patch("kachlog.utils.actual_version")
    def test_get_new_release_version_patch(self, mock_actual_version):
        mock_actual_version.return_value = (
            "1.1.1",
            {"major": 1, "minor": 1, "patch": 1},
        )
        CL = ChangelogUtils()
        CL.CHANGELOG = self.cl.CHANGELOG
        CL.initialize_changelog_file()
        self.assertEqual(CL.get_new_release_version("patch"), "1.1.2")

    @patch("kachlog.utils.actual_version")
    def test_get_new_release_version_minor(self, mock_actual_version):
        mock_actual_version.return_value = (
            "1.1.1",
            {"major": 1, "minor": 1, "patch": 1},
        )
        CL = ChangelogUtils()
        CL.CHANGELOG = self.cl.CHANGELOG
        CL.initialize_changelog_file()
        self.assertEqual(CL.get_new_release_version("minor"), "1.2.0")

    @patch("kachlog.utils.actual_version")
    def test_get_new_release_version_major(self, mock_actual_version):
        mock_actual_version.return_value = (
            "1.1.1",
            {"major": 1, "minor": 1, "patch": 1},
        )
        CL = ChangelogUtils()
        CL.CHANGELOG = self.cl.CHANGELOG
        CL.initialize_changelog_file()
        self.assertEqual(CL.get_new_release_version("major"), "2.0.0")

    def test_release(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "new": ["added new feature"],
                "changes": ["changes feature"],
                "fixes": ["fixed bug 1"],
                "breaks": [],
            },
            "0.3.2": {
                "metadata": {
                    "version": "0.3.2",
                    "release_date": "2017-06-09",
                    "semantic_version": {"major": 0, "minor": 3, "patch": 2},
                }
            },
        }
        original_unreleased = sample_data["unreleased"]
        version = release(sample_data)
        self.assertEqual("0.4.0", version)
        self.assertEqual(len(sample_data.keys()), 3)
        self.assertIn("0.3.2", sample_data)
        self.assertIn("0.4.0", sample_data)
        for change in original_unreleased:
            if change == "metadata":
                continue
            self.assertEqual(original_unreleased[change], sample_data[version][change])

    def test_release_lower_version(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "new": ["added new feature"],
                "changes": ["changes feature"],
                "fixes": ["fixed bug 1"],
                "breaks": [],
            },
            "0.3.2": {
                "metadata": {
                    "version": "0.3.2",
                    "release_date": "2017-06-09",
                    "semantic_version": {"major": 0, "minor": 3, "patch": 2},
                }
            },
        }
        with self.assertRaises(InvalidSemanticVersion):
            release(sample_data, "0.1.0")


class ChangelogFileOperationTestCase(unittest.TestCase):
    def setUp(self):
        self.CL = ChangelogUtils()
        self.CL.CHANGELOG = "TEST_CHANGELOG.md"

    def tearDown(self):
        try:
            os.remove(self.CL.CHANGELOG)
        except Exception:
            pass

    def test_initialize_changelog_file(self):
        self.CL.initialize_changelog_file()
        self.assertTrue(os.path.isfile("TEST_CHANGELOG.md"))

    def test_initialize_changelog_file_exists(self):
        self.CL.initialize_changelog_file()
        self.assertTrue(os.path.isfile("TEST_CHANGELOG.md"))
        message = self.CL.initialize_changelog_file()
        self.assertEqual(message, "TEST_CHANGELOG.md already exists")

    def test_get_changelog_data(self):
        self.CL.initialize_changelog_file()
        self.assertTrue(len(self.CL.data) >= 1)

    def test_get_changelog_no_file(self):
        with self.assertRaises(ChangelogDoesNotExistError):
            print(self.CL.data)

    def test_write_changelog(self):
        self.CL.initialize_changelog_file()
        with open(self.CL.CHANGELOG, "r", encoding="utf8") as f:
            original = len(f.readlines())
        self.CL.data["unreleased"].update({"changed": ["test"]})
        self.CL.write_changelog()
        with open(self.CL.CHANGELOG, "r", encoding="utf8") as f:
            modified = len(f.readlines())
        # new lines are '\n','### Changed\n', '- test\n'
        self.assertEqual(original + 3, modified)

    def test_cut_release(self):
        self.CL.initialize_changelog_file()
        self.CL.update_section("added", "this is a test")
        self.CL.cut_release(self.CL.get_new_release_version())
        with open(self.CL.CHANGELOG, "r", encoding="utf8") as ch:
            data = ch.readlines()
        self.assertTrue("## [Unreleased]\n" in data)
        self.assertTrue(f"## [0.1.0] - {date.today().isoformat()}\n" in data)
        # no empty sections in `Unreleased`
        below_unreleased = False
        for line in data:
            if not below_unreleased and line == "## [Unreleased]\n":
                below_unreleased = True
                continue
            if below_unreleased:
                if line.startswith("## ["):
                    # new version
                    break
                self.assertEqual("\n", line)

        with self.assertRaises(InvalidSemanticVersion):
            self.CL.cut_release("non-semantic.version")

        self.CL.update_section("removed", "removed a thing")
        # test adding empty sections in `Unreleased` on demand
        self.CL.cut_release(self.CL.get_new_release_version(), True)
        with open(self.CL.CHANGELOG, "r") as ch:
            data2 = ch.readlines()
        self.assertTrue("## [Unreleased]\n" in data2)
        # basic sections like `Added`, `Fixed`, `Changed`, `Security`,...
        # are inserted into `Unreleased` version and initialy they are empty
        in_unreleased = False
        in_section = False
        for line in data2:
            if line == "## [Unreleased]\n":
                in_unreleased = True
                continue
            if in_unreleased:
                if line.startswith("###"):
                    in_section = True
                    continue
                if line.startswith("## ["):
                    # new version
                    break
                if in_section:
                    self.assertEqual("\n", line)

    def test_cut_release_works_with_beta_headers(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "new": ["added new feature"],
                "changes": ["changes feature"],
                "fixes": ["fixed bug 1"],
                "breaks": [],
            },
            "0.3.2": {
                "metadata": {
                    "version": "0.3.2",
                    "release_date": "2017-06-09",
                    "semantic_version": {"major": 0, "minor": 3, "patch": 2},
                }
            },
        }
        with patch.object(
            ChangelogUtils, "data", new_callable=PropertyMock, return_value=sample_data
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.CL.CHANGELOG
            # adding empty sections in unreleased to test if new formats exists
            CL.cut_release(CL.get_new_release_version(), True)
        with open(CL.CHANGELOG, "r", encoding="utf8") as ch:
            data = ch.readlines()
        self.assertTrue("## [Unreleased]\n" in data)
        self.assertTrue(f"## [0.4.0] - {date.today().isoformat()}\n" in data)
        # The beta headings still exist
        self.assertTrue("### New\n" in data)
        self.assertTrue("### Changes\n" in data)
        self.assertTrue("### Fixes\n" in data)
        # The new headings exist
        self.assertTrue("### Added\n" in data)
        self.assertTrue("### Changed\n" in data)
        self.assertTrue("### Deprecated\n" in data)
        self.assertTrue("### Removed\n" in data)
        self.assertTrue("### Fixed\n" in data)
        self.assertTrue("### Security\n" in data)

    def test_get_changes(self):
        self.CL.initialize_changelog_file()
        self.assertIn("## [Unreleased]\n", self.CL.get_changes("unreleased"))
        with self.assertRaises(InvalidSemanticVersion):
            self.CL.get_changes("non-semantic-version")
        with self.assertRaises(VersionDoesNotExistError):
            self.CL.get_changes("1.0.0")  # non existing version

    def test_dict_from_file(self):
        self.CL.initialize_changelog_file()
        with open(self.CL.CHANGELOG, "r", encoding="utf8") as ch:
            data = ch.readlines()
        # different input types for to_dict
        self.assertEqual(to_dict(data), to_dict(self.CL.CHANGELOG))

    def test_parse_file_to_dict(self):
        with open(self.CL.CHANGELOG, "w", encoding="utf8") as ch:
            ch.write(CHANGELOG_DATA)
        data = self.CL.data
        self.assertEqual(
            data["1.0.1"]["metadata"]["url"],
            "https://github.test_url/test_project/compare/v1.0.0...v1.0.1",
        )
        self.assertEqual(data["1.0.1"]["uncategorized"], ["uncategorized log"])

    def test_parse_file_to_raw_dict(self):
        with open(self.CL.CHANGELOG, "w", encoding="utf8") as ch:
            ch.write(CHANGELOG_DATA)
        raw_dict = to_raw_dict(self.CL.CHANGELOG)
        self.assertEqual(len(raw_dict.keys()), 4)  # 4 versions released
        self.assertEqual(
            raw_dict["1.0.1"]["raw"],
            "- uncategorized log\n### Fixed\n- Bug fix 1 (1.0.1)\n- sub bug 1\n- sub bug 2\n- Bug fix 2 (1.0.1)\n",
        )

    def test_uncategorized_to_file(self):
        sample_data = {
            "unreleased": {
                "metadata": {"version": "unreleased", "release_date": None},
                "new": ["added new feature"],
                "changes": ["changes feature"],
                "fixes": ["fixed bug 1"],
                "breaks": [],
            },
            "0.3.2": {
                "metadata": {
                    "version": "0.3.2",
                    "release_date": "2017-06-09",
                    "semantic_version": {"major": 0, "minor": 3, "patch": 2},
                    "url": "https://link_to/tag/0.3.2",
                },
                "uncategorized": [
                    "lousy changelog file",
                    "i don't care enough to do this right",
                ],
            },
        }
        output = from_dict(sample_data)
        lines = output.split("\n")
        self.assertIn("## [0.3.2] - 2017-06-09", lines)
        self.assertIn("* lousy changelog file", lines)
        self.assertIn("* i don't care enough to do this right", lines)
        self.assertIn("[0.3.2]: https://link_to/tag/0.3.2", lines)
        in_version = False
        uncategorized = []
        for line in lines:
            if line == "## [0.3.2] - 2017-06-09":
                in_version = True
                continue
            if in_version:
                if (
                    line == "* lousy changelog file"
                    or line == "* i don't care enough to do this right"
                ):
                    uncategorized.append(line.lstrip("* "))
        self.assertEqual(sample_data["0.3.2"]["uncategorized"], uncategorized)

    def test_nonexistent_unreleased_section(self):
        sample_data = [
            "## [0.3.2] - 2017-06-09\n",
            "\n",
            "### Added\n",
            "* added feature x\n",
            "\n",
            "### Fixed\n",
            "* fixed bug 1\n",
            "\n",
            "### Removed\n",
            "\n",
            "\n",
        ]
        with patch.object(
            ChangelogUtils,
            "data",
            new_callable=PropertyMock,
            return_value=_to_dict(sample_data, show_unreleased=True),
        ):
            CL = ChangelogUtils()
            CL.CHANGELOG = self.CL.CHANGELOG
            # check generating changes for nonexistent `unreleased` version
            CL.get_changes("unreleased")
            # update section `Added` in `unreleased` which doesn't exist
            CL.update_section("Added", "this will be the new feature")
            # cut release with no `unreleased` version
            CL.cut_release(CL.get_new_release_version(), True)
