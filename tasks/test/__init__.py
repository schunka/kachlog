"""
Run Test Suite
"""

from invoke import task


@task
def test(context):
    """
    Runs Test Suite
    """
    context.run("coverage run --source=src -m pytest --junitxml=report.xml")
    context.run("coverage xml")
    context.run("coverage report")
