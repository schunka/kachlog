"""
Release Task
"""

import os

from invoke import Exit, task

from kachlog._version import __version__
from kachlog.utils import ChangelogUtils


@task
def clean(context):
    """
    Clean dist and build folders
    :param context:
    :return:
    """
    context.run("rm -rf dist")
    context.run("rm -rf build")


@task
def check_version(context):
    """
    Runs changelog command to update changelog
    """
    pkg_ver = __version__
    ch_ver = ChangelogUtils().get_current_version()
    tag_ver = os.getenv("CI_COMMIT_TAG")
    if ch_ver != pkg_ver:
        raise Exit(
            f"Aborting! Version tag mismatch:\n"
            f"changelog -> {ch_ver}\npackage -> {pkg_ver}"
        )
    if ch_ver != tag_ver:
        raise Exit(
            f"Aborting! Version tag mismatch:\n"
            f"changelog -> {ch_ver}\ntag -> {tag_ver}"
        )


@task(pre=[clean])
def build(context):
    """
    Build the current python version
    """
    context.run("python setup.py sdist bdist_wheel")


def publish_staging(context):
    """
    Publish the package on gitlab project repository
    """
    context.run(
        f"twine upload -u gitlab-ci-token -p {os.getenv('CI_JOB_TOKEN')} "
        "--repository-url "
        f"{os.getenv('CI_API_V4_URL')}/projects/{os.getenv('CI_PROJECT_ID')}/packages/pypi dist/*"
    )


def publish_stable(context):
    """
    Publish the package on pypi
    """
    context.run(f"twine upload -u __token__ -p {os.getenv('PYPI_PASSWORD')} dist/*")


@task(pre=[check_version], optional=["stable"])
def release(context, stable=False):
    """
    Release a new version of kachlog
    """
    if stable:
        publish_stable(context)
    else:
        publish_staging(context)
