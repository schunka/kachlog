from invoke import Collection

from tasks.lint import lint
from tasks.release import build, release
from tasks.test import test

ns = Collection()
ns.add_task(test)
ns.add_task(lint)
ns.add_task(build)
ns.add_task(release)
