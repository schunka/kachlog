# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed
- changes in prospector linting
- gitlab ci test coverage update

## [1.2.1] - 2022-04-28

### Fixed
- minimal python version requirement

## [1.2.0] - 2022-03-24

### Added
- pre-commit with black, yamllint and pylint checks

### Fixed
- missing `[Unreleased]` version is treated like empty one

## [1.1.0] - 2021-12-15

### Added
- flag --sections for `init` and `release` commands
- better output formating, eg. new lines...

### Fixed
- test coverage

## [1.0.0] - 2021-12-07

### Added
- First working release

[Unreleased]: https://gitlab.com/schunka/kachlog/-/compare/1.2.1...main
[1.2.1]: https://gitlab.com/schunka/kachlog/-/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.com/schunka/kachlog/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/schunka/kachlog/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/schunka/kachlog/-/releases/1.0.0
