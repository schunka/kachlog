# pylint: disable=missing-module-docstring
import os
import sys

from setuptools import find_packages, setup

sys.path.insert(0, "src")  # using itself for getting own version
from kachlog.utils import ChangelogUtils  # pylint: disable=wrong-import-position

v = ChangelogUtils().get_current_version()

dev_requirements = [
    "coverage",
    "pytest",
    "invoke",
    "mock",
    "prospector",
    "twine",
    "wheel",
    "pre-commit",
]

setup(
    name="KaChLog",
    description="Command line interface for managing CHANGELOG.md files",
    long_description=open("README.md", "r").read(),
    long_description_content_type="text/markdown",
    version=os.getenv("MODULE_VERSION_ID", v),
    author="Ales Jirasek",
    author_email="schunkac@gmail.com",
    packages=find_packages("src"),
    package_dir={"": "src"},
    url="https://gitlab.com/schunka/kachlog",
    license="MIT",
    install_requires=["click"],
    extras_require={"dev": dev_requirements},
    entry_points={
        "console_scripts": [
            "changelog = kachlog.commands:cli",
            "kachlog = kachlog.commands:cli",
            "cl = kachlog.commands:cli",
            "kl = kachlog.commands:cli",
        ]
    },
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Topic :: Documentation",
        "Topic :: Software Development :: Build Tools",
        "Topic :: Software Development :: Documentation",
        "Topic :: Utilities",
    ],
)
